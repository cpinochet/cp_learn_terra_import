## Replace this file with your configuration from following the scenario.
##
## Final configuration:
#
# resource "docker_image" "nginx" {
#   name = "nginx:latest"
# }
#
# # docker_container.web:
# resource "docker_container" "web" {
#   name  = "hashicorp-learn"
#
#   image = docker_image.nginx.latest
#
#   ports {
#     external = 8081
#     internal = 80
#   }
# }

resource "docker_image" "nginx" {
  name = "nginx:latest"
}


resource "docker_container" "web" {
  name  = "hashicorp-learn"
  image = docker_image.nginx.latest
  # image = "sha256:f2f70adc5d89aa922836e9cc6801980a12a7ff9012446cc6edf52ef8798a67bd"
  env = []
  ports {
    external = 8081
    internal = 80
  }
}
