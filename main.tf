terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.16.0"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

# Pulls the image
resource "docker_image" "server" {
  name = "cpinochet/multi-server:latest"
}

# Create a container
resource "docker_container" "server" {
  name  = "server-test"
  image = docker_image.server.latest
}